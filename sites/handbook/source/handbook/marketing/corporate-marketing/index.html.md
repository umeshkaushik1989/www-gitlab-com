---
layout: handbook-page-toc
title: Corporate Marketing at GitLab
description: For strategies and administration of corporate marketing at GitLab
twitter_image: null
twitter_image_alt: GitLab's Corporate Marketing Team Handbook
twitter_site: gitlab
twitter_creator: gitlab
---
## Welcome to the Corporate Marketing Handbook
{:.no_toc}

The Corporate Marketing team includes Corporate Events, Corporate Communications (PR, Executive Communications and Social Media), All-Remote Marketing, and the Community Team. Corporate Marketing is ultimately responsible for creating awareness and driving top-of-funnel interest in GitLab. We do this by driving conversations in the communications channels that accelerate our objectives and by developing an integrated communication strategy that is executed globally.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How We Work

### Project Management

There is an initiative underway to simplify Project Management across all of Marketing.

In the meantime, we have implemented a few practices to help us get organized:

- Add weekly milestones to issues based on the week you expect to work on the issue (format is `Fri: Apr 17, 2020`)
- Move issues forward into future milestones, or place them in the `backlog` milestone if they're no longer planned
- We close out the week's milestone each week, and issues will be moved forward if they are not closed out
- You can use labels however you like, they are a tool for you and your team to filter/sort issues

### OKRs

Each quarter, we create cascading OKRs from:

- [CEO OKRs](/company/okrs/#most-recent-okrs)
- [Marketing OKRs](/handbook/marketing/#most-recent-okrs)

#### Most recent OKRs

  * [FY22 Q4](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/160)
  * [FY22 Q3](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/136)
  * [FY22 Q2](https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/92)
  * [Previous OKRs](/handbook/marketing/corporate-marketing/#okr-archive)

## GitLab brand

GitLab empowers everyone through knowledge access, job access, and the DevOps platform. This is captured in our [GitLab brand](/company/brand/).

## Brand personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com...across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

1. **Human**: We [write like we talk](/handbook/values/#directness). We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
1. **Competent**: We are [highly accomplished](/handbook/values/#results), and we communicate with conviction. We are [efficient](/handbook/values/#efficiency) at everything we do.
1. **Quirky**: We embrace [diversity of opinion](/handbook/values/#diversity-inclusion). We embrace new ideas based on their merit, even if they defy commonly held norms.
1. **Humble**: We care about helping those around us [achieve great things](/handbook/values/#customer-results) more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were `quirky` without being `human` we could come across as eccentric. If we were `competent` without being `humble` we could come across as arrogant.

GitLab has a [higher purpose](/company/mission/#mission) to make it so that **everyone can contribute**. We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

### Tone of Voice

See [Brand Guidelines](https://design.gitlab.com/brand/overview/#tone-of-voice).

### Design

Read more about our Brand Guidelines in the [Brand Activation](/handbook/marketing/corporate-marketing/brand-activation/brand-standards/#brand-guidelines) handbook.

### About

#### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab-ce/)
with a large community of contributors.

#### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is
an active participant in our community (see our [stewardship of GitLab CE](/company/stewardship/)
for more information), as well as offering GitLab, a product (see below).

#### GitLab the product

GitLab is a complete DevOps platform, delivered as a single application. See the
[product elevator pitch](/handbook/marketing/strategic-marketing/messaging/)
for additional messaging.

- - -

## Speakers

For resources for GitLab team members who are planning on attending events or speaking at conferences, see [Speaker Resources](/handbook/marketing/corporate-marketing/speaking-resources/). To join the [GitLab Speakers Bureau](/speakers/), see the [Developer Evangelist page on the Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).

- - -

## Corporate Events

### Mission Statement

To learn more about the Corporate Events team, its purview, and how we work, please see the [Corporate Event Marketing at GitLab](/handbook/marketing/corporate-marketing/corp-event-marketing/) handbook page.

- The mission of the Corporate Events Team is to:
    - Tell the GitLab story through engaging events
    - Make customers the hero
    - Build lasting and trusting vendor and internal relationships

See our [handbook page](/handbook/marketing/corporate-marketing/corp-event-marketing/) for more details.


- - -

## Swag/ [Merchandise Handling](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/)

For merch team, see more details on handling and fulfillment specific on the [merchandise handling page](https://about.gitlab.com/handbook/marketing/corporate-marketing/merchandise-handling/)
All swag requests, creation and vendor selection is handled by the Corporate Events Marketing team.

- We aim to have our swag delight and/or be useful. We want to create swag that is versatile, easy to store and transport.
- As a remote company with team members in over 60 countries - our swag often has to go on miraculous journeys.
- With this in mind we try to ship things that are durable, light and that will unlikely get stuck in customs.
- We strive to make small batch, limited edition and themed swag for the community to collect.
- Larger corporate events will have custom tanuki stickers in small runs, only available at the specific event.
- Region specific sticker designs are produced quarterly.
- Our goal is to make swag self-serve => [web shop](https://gitlab.myshopify.com/). Please note our swag store is not intended for ordering and expensing back items purchased, see below on how to order swag if you are internal to GitLab.

- - -

## All-Remote Marketing

Please consult the [All-Remote Marketing Handbook](/handbook/marketing/corporate-marketing/all-remote/).

## Corporate Communications (Public Relations, Social Media, Executive Communications and Internal Communications)

Please consult the [Corporate Communications Handbook](/handbook/marketing/corporate-marketing/corporate-communications/).

### Social Marketing and Social Media

Please consult the [Social Marketing Handbook](/handbook/marketing/corporate-marketing/social-marketing/).

# OKR Archive

FIXME

- - -

Return to the main [GitLab Marketing Handbook](/handbook/marketing/).
