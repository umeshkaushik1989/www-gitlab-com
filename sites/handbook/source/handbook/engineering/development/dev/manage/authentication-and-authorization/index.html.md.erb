---
layout: handbook-page-toc
title: "Authentication and Authorization Group"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Manage:Authentication and Authorization
{: #welcome}


### How we work

* In accordance with our [GitLab values](/handbook/values/).
* Transparently: nearly everything is public, we record/livestream meetings whenever possible.
* We get a chance to work on the things we want to work on.
* Everyone can contribute; no silos.
  * The goal is to have product give engineering and design the opportunity to be involved with direction and issue definition from the very beginning.
* We do an optional, asynchronous daily stand-up in our group stand-up channel:
  * Manage:Authentication and Authorization [#g_manage_auth_daily](https://gitlab.slack.com/archives/C01311Z0LDD)

#### Keeping up-to-date

We create a weekly issue to inform the team members about the company or team updates, to share important links or to be informed about the team availability. Creation of the issue is the responsobility of an Engineering Manager, who can use an issue template located in the [Manage/Auth repo](https://gitlab.com/gitlab-org/manage/authentication-and-authorization/discussion). All weekly updates can be found in the project issue list [filtered by weekly update label.](https://gitlab.com/gitlab-org/manage/authentication-and-authorization/discussion/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=weekly%20update&first_page_size=20)

#### Prioritization
<%= partial "handbook/engineering/development/dev/manage/prioritization.erb", locals: { filter_value: 'authentication and authorization' } %>

**Monthly cross-functional dashboard Review**

We create a monthly issue that is assigned to all counterparts. The issue has to be created manually by an Engineering Manager, but we have an issue template in [Authentication & Authorization discussion repo](https://gitlab.com/gitlab-org/manage/authentication-and-authorization/discussion).

Besides answering to the questions in the issue we use [cross-functional issues board](https://gitlab.com/groups/gitlab-org/-/boards/4453752?not[label_name][]=UX&not[milestone_title]=15.2&label_name[]=group%3A%3Aauthentication%20and%20authorization) for prioritisation. PM is responsible for Feature, EM for Maintenance and SET for Bugs column.

You can read more about this process on the [respective handbook page](/handbook/cross-functional-prioritization).


#### Organizing the work

<%= partial("handbook/engineering/development/dev/manage/organizing_work.erb") %>

#### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. If the scope of work of a given issue touches several disciplines (docs, design, frontend, backend, etc.) and involves significant complexity across them, consider creating separate issues for each discipline (see [an example](https://gitlab.com/gitlab-org/gitlab/-/issues/9288)). 

When estimating development work, please assign an issue an appropriate weight:

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests affected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. We should challenge ourselves to break this issue into smaller pieces. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. These issues will often need further investigation or discovery before being ~workflow::ready for development and we will likely benefit from multiple, smaller issues. |

We do not provide estimates greater than 8 - in a case an issue is bigger, we will split the issue or reduce its scope. 

##### Estimatiom process

When a PM or EM wants to schedule an issue without `weight` for a release, they should ping Engineers directly asking for the estimation and assign the issue to them. Ideally the distribution between Enginners across the group is even.

When an Engineer is done with their estimates they should ask another team member to review their weight. This makes it harder to miss a significant aspect of an issue. If the first Engineer is confident (eg. has deep expertise in the problem or the issue is trivial) they can skip this step.

Once the weight of the issue is final, the Engineer will ping the person who asked for the estimation. If the issue has weight 5 or less they should put `~"workflow::ready for development"` label on it.

If the issue has weight more than 5 (or 5 but it seems it might be split into multiple issues) the Engineer will suggest how to split it and ping the person who asked for the estimation. If the Engineer is clear about the splitting they should proactivelly split the issue themself, put estimations on the child issues and leave a comment on the parent one. If it results to multiple issues, creating an epic should be considered.

#### Planning

<%= partial("handbook/engineering/development/dev/manage/planning.erb") %>

#### During a release

<%= partial("handbook/engineering/development/dev/manage/release.erb") %>

#### Release posts

<%= partial("handbook/engineering/development/dev/manage/release_posts.erb") %>

#### Proof-of-concept MRs

<%= partial("handbook/engineering/development/dev/manage/pocs.erb") %>

### Working on unscheduled issues

<%= partial("handbook/engineering/development/dev/manage/unscheduled_issues.erb") %>


### Additional considerations

<%= partial("handbook/engineering/development/dev/manage/access_processes.erb") %>


## Meetings

Although we have a bias for asynchronous communication, synchronous meetings are necessary and should adhere to our [communication guidelines](/handbook/communication/#video-calls). Some regular meetings that take place in Manage are:

| Frequency | Meeting                              | DRI         | Possible topics                                                                                        |
|-----------|--------------------------------------|-------------|--------------------------------------------------------------------------------------------------------|
| Weekly    | Group-level meeting                  | Backend Engineering Managers | Ensure current release is on track by walking the board, unblock specific issues                       |
| Monthly   | Planning meetings                    | Product Managers         | See [Planning](/handbook/engineering/development/dev/manage/#planning) section |

For one-off, topic specific meetings, please always consider recording these calls and sharing them (or taking notes in a [publicly available document](https://docs.google.com/document/d/1kE8udlwjAiMjZW4p1yARUPNmBgHYReK4Ks5xOJW6Tdw/edit)).

Agenda documents and recordings can be placed in the [shared Google drive](https://drive.google.com/drive/u/0/folders/0ALpc3GhrDkKwUk9PVA) (internal only) as a single source of truth.

Meetings that are not 1:1s or covering confidential topics should be added to the Manage Shared calendar.

All meetings should have an agenda prepared at least 12 hours in advance. If this is not the case, you are not obligated to attend the meeting. Consider meetings canceled if they do not have an agenda by the start time of the meeting.

## Group Members

Auth group members who are part of the [Authentication and Authorization group](https://gitlab.com/groups/gitlab-org/manage/authentication-and-authorization/) can be `@` mentioned on GitLab with `@gitlab-org/manage/authentication-and-authorization`.

The following people are permanent members of the group:

<%= stable_counterparts(role_regexp: /Manage:Authentication and Authorization/) %>

## Dashboards

- [SAML SSO Usage](https://app.periscopedata.com/app/gitlab/636494/Dev:-Manage:-Access-SAML-SSO-Usage)
- [Backend overview](https://app.periscopedata.com/app/gitlab/695525/Manage::Access-Backend-Overview)
- [Usage ping](https://app.periscopedata.com/app/gitlab/857665/Manage:Access---Usage-Ping)
- [Error Budget](https://dashboards.gitlab.net/d/stage-groups-detail-authentication_and_a/stage-groups-authentication-and-authorization-group-error-budget-detail?orgId=1&from=now-2d&to=now)

## Links and resources
{: #links}

<%= partial("handbook/engineering/development/dev/manage/shared_links.erb") %>
* [Milestone retrospectives](https://gitlab.com/gl-retrospectives/manage-stage/authentication-and-authorization/-/issues)
* Our Slack channels
  * Manage:Authentication and Authorization [#g_manage_auth](https://gitlab.slack.com/archives/CLM1D8QR0)
  * Daily standups [#g_manage_auth_daily](https://gitlab.slack.com/archives/C01311Z0LDD)
* Issue boards
  * [Release workflow board](https://gitlab.com/gitlab-org/gitlab/-/boards/4406395) 
  * [Security issues board](https://gitlab.com/gitlab-org/gitlab/-/boards/4260654?label_name%5B%5D=devops%3A%3Amanage&label_name%5B%5D=group%3A%3Aauthentication%20and%20authorization&label_name%5B%5D=bug%3A%3Avulnerability)
  * [Cross-functional prioritisation board](https://gitlab.com/groups/gitlab-org/-/boards/4453752)
