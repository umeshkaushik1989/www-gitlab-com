---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 3100 |
| [@leipert](https://gitlab.com/leipert) | 2 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 1510 |
| [@sabrams](https://gitlab.com/sabrams) | 4 | 1300 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 980 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 6 | 940 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 7 | 900 |
| [@brodock](https://gitlab.com/brodock) | 8 | 800 |
| [@.luke](https://gitlab.com/.luke) | 9 | 640 |
| [@kassio](https://gitlab.com/kassio) | 10 | 600 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 11 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 12 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 13 | 600 |
| [@ifarkas](https://gitlab.com/ifarkas) | 14 | 520 |
| [@ratchade](https://gitlab.com/ratchade) | 15 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 16 | 500 |
| [@jlear](https://gitlab.com/jlear) | 17 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 18 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 19 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 20 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 21 | 500 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 22 | 500 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 23 | 490 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 24 | 480 |
| [@toupeira](https://gitlab.com/toupeira) | 25 | 480 |
| [@kerrizor](https://gitlab.com/kerrizor) | 26 | 480 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 27 | 460 |
| [@garyh](https://gitlab.com/garyh) | 28 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 29 | 440 |
| [@stanhu](https://gitlab.com/stanhu) | 30 | 400 |
| [@xanf](https://gitlab.com/xanf) | 31 | 400 |
| [@markrian](https://gitlab.com/markrian) | 32 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 33 | 300 |
| [@dzubova](https://gitlab.com/dzubova) | 34 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 35 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 36 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 37 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 38 | 300 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 39 | 290 |
| [@ahegyi](https://gitlab.com/ahegyi) | 40 | 260 |
| [@vshushlin](https://gitlab.com/vshushlin) | 41 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 42 | 220 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 43 | 210 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 44 | 200 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 45 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 46 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 47 | 190 |
| [@10io](https://gitlab.com/10io) | 48 | 170 |
| [@pshutsin](https://gitlab.com/pshutsin) | 49 | 140 |
| [@avielle](https://gitlab.com/avielle) | 50 | 140 |
| [@mwoolf](https://gitlab.com/mwoolf) | 51 | 130 |
| [@alexpooley](https://gitlab.com/alexpooley) | 52 | 130 |
| [@egrieff](https://gitlab.com/egrieff) | 53 | 120 |
| [@lauraX](https://gitlab.com/lauraX) | 54 | 120 |
| [@dblessing](https://gitlab.com/dblessing) | 55 | 120 |
| [@mattkasa](https://gitlab.com/mattkasa) | 56 | 100 |
| [@drew](https://gitlab.com/drew) | 57 | 90 |
| [@dmakovey](https://gitlab.com/dmakovey) | 58 | 80 |
| [@mbobin](https://gitlab.com/mbobin) | 59 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 60 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 61 | 80 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 62 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 63 | 60 |
| [@minac](https://gitlab.com/minac) | 64 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 65 | 60 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 66 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 67 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 68 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 69 | 60 |
| [@cngo](https://gitlab.com/cngo) | 70 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 71 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 72 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 73 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 74 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 75 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 76 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 77 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 78 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 79 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 80 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 81 | 30 |
| [@subashis](https://gitlab.com/subashis) | 82 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 83 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 84 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 85 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 86 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 87 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 88 | 30 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 89 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 90 | 20 |
| [@terrichu](https://gitlab.com/terrichu) | 91 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@greg](https://gitlab.com/greg) | 2 | 500 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 3 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 4 | 440 |
| [@f_santos](https://gitlab.com/f_santos) | 5 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 6 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 7 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 8 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 9 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 10 | 30 |
| [@fneill](https://gitlab.com/fneill) | 11 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 2 | 400 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 3 | 400 |
| [@vburton](https://gitlab.com/vburton) | 4 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@mehulsharma](https://gitlab.com/mehulsharma) | 2 | 500 |
| [@feistel](https://gitlab.com/feistel) | 3 | 400 |
| [@tnir](https://gitlab.com/tnir) | 4 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 5 | 300 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 6 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 7 | 200 |
| [@zined](https://gitlab.com/zined) | 8 | 200 |
| [@trakos](https://gitlab.com/trakos) | 9 | 200 |

## FY23-Q2

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 1900 |
| [@sabrams](https://gitlab.com/sabrams) | 2 | 1300 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 3 | 900 |
| [@mc_rocha](https://gitlab.com/mc_rocha) | 4 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 5 | 480 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 6 | 430 |
| [@ifarkas](https://gitlab.com/ifarkas) | 7 | 420 |
| [@markrian](https://gitlab.com/markrian) | 8 | 400 |
| [@dzubova](https://gitlab.com/dzubova) | 9 | 300 |
| [@viktomas](https://gitlab.com/viktomas) | 10 | 300 |
| [@joe-shaw](https://gitlab.com/joe-shaw) | 11 | 300 |
| [@ajwalker](https://gitlab.com/ajwalker) | 12 | 300 |
| [@tle_gitlab](https://gitlab.com/tle_gitlab) | 13 | 300 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 14 | 260 |
| [@ahegyi](https://gitlab.com/ahegyi) | 15 | 260 |
| [@toupeira](https://gitlab.com/toupeira) | 16 | 240 |
| [@vshushlin](https://gitlab.com/vshushlin) | 17 | 240 |
| [@dbalexandre](https://gitlab.com/dbalexandre) | 18 | 220 |
| [@pgascouvaillancourt](https://gitlab.com/pgascouvaillancourt) | 19 | 200 |
| [@brodock](https://gitlab.com/brodock) | 20 | 200 |
| [@alberts-gitlab](https://gitlab.com/alberts-gitlab) | 21 | 200 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 22 | 180 |
| [@avielle](https://gitlab.com/avielle) | 23 | 140 |
| [@drew](https://gitlab.com/drew) | 24 | 90 |
| [@kerrizor](https://gitlab.com/kerrizor) | 25 | 80 |
| [@seanarnold](https://gitlab.com/seanarnold) | 26 | 80 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 27 | 80 |
| [@.luke](https://gitlab.com/.luke) | 28 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 29 | 70 |
| [@10io](https://gitlab.com/10io) | 30 | 70 |
| [@egrieff](https://gitlab.com/egrieff) | 31 | 60 |
| [@allison.browne](https://gitlab.com/allison.browne) | 32 | 60 |
| [@tkuah](https://gitlab.com/tkuah) | 33 | 60 |
| [@serenafang](https://gitlab.com/serenafang) | 34 | 60 |
| [@fabiopitino](https://gitlab.com/fabiopitino) | 35 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 36 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 37 | 60 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 38 | 60 |
| [@abdwdd](https://gitlab.com/abdwdd) | 39 | 60 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 40 | 40 |
| [@manojmj](https://gitlab.com/manojmj) | 41 | 40 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 42 | 40 |
| [@mhenriksen](https://gitlab.com/mhenriksen) | 43 | 40 |
| [@alexpooley](https://gitlab.com/alexpooley) | 44 | 30 |
| [@mbobin](https://gitlab.com/mbobin) | 45 | 30 |
| [@fjsanpedro](https://gitlab.com/fjsanpedro) | 46 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 47 | 30 |
| [@acroitor](https://gitlab.com/acroitor) | 48 | 30 |
| [@terrichu](https://gitlab.com/terrichu) | 49 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@ashmckenzie](https://gitlab.com/ashmckenzie) | 1 | 600 |
| [@godfat-gitlab](https://gitlab.com/godfat-gitlab) | 2 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 3 | 40 |
| [@fneill](https://gitlab.com/fneill) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@kmcknight](https://gitlab.com/kmcknight) | 1 | 500 |
| [@mbruemmer](https://gitlab.com/mbruemmer) | 2 | 400 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@mehulsharma](https://gitlab.com/mehulsharma) | 1 | 500 |
| [@tnir](https://gitlab.com/tnir) | 2 | 400 |
| [@kyrie.31415926535](https://gitlab.com/kyrie.31415926535) | 3 | 300 |
| [@trakos](https://gitlab.com/trakos) | 4 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 2000 |
| [@tkuah](https://gitlab.com/tkuah) | 2 | 1450 |
| [@vitallium](https://gitlab.com/vitallium) | 3 | 1200 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 900 |
| [@kassio](https://gitlab.com/kassio) | 5 | 600 |
| [@brodock](https://gitlab.com/brodock) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 560 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 540 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@djadmin](https://gitlab.com/djadmin) | 18 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 19 | 460 |
| [@garyh](https://gitlab.com/garyh) | 20 | 440 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 22 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 23 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 24 | 400 |
| [@xanf](https://gitlab.com/xanf) | 25 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 26 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 27 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 28 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 29 | 150 |
| [@pshutsin](https://gitlab.com/pshutsin) | 30 | 140 |
| [@10io](https://gitlab.com/10io) | 31 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 32 | 100 |
| [@alexpooley](https://gitlab.com/alexpooley) | 33 | 100 |
| [@ifarkas](https://gitlab.com/ifarkas) | 34 | 100 |
| [@dmakovey](https://gitlab.com/dmakovey) | 35 | 80 |
| [@rmarshall](https://gitlab.com/rmarshall) | 36 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 37 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 38 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 39 | 60 |
| [@minac](https://gitlab.com/minac) | 40 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 41 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 42 | 60 |
| [@pedropombeiro](https://gitlab.com/pedropombeiro) | 43 | 60 |
| [@lauraX](https://gitlab.com/lauraX) | 44 | 60 |
| [@nick.thomas](https://gitlab.com/nick.thomas) | 45 | 60 |
| [@bala.kumar](https://gitlab.com/bala.kumar) | 46 | 60 |
| [@dblessing](https://gitlab.com/dblessing) | 47 | 60 |
| [@cngo](https://gitlab.com/cngo) | 48 | 60 |
| [@ebaque](https://gitlab.com/ebaque) | 49 | 50 |
| [@mbobin](https://gitlab.com/mbobin) | 50 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 51 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 52 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 53 | 40 |
| [@balasankarc](https://gitlab.com/balasankarc) | 54 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 55 | 30 |
| [@subashis](https://gitlab.com/subashis) | 56 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 57 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 58 | 30 |
| [@furkanayhan](https://gitlab.com/furkanayhan) | 59 | 30 |
| [@robotmay_gitlab](https://gitlab.com/robotmay_gitlab) | 60 | 30 |
| [@shinya.maeda](https://gitlab.com/shinya.maeda) | 61 | 30 |
| [@digitalmoksha](https://gitlab.com/digitalmoksha) | 62 | 30 |
| [@ealcantara](https://gitlab.com/ealcantara) | 63 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 64 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 65 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 66 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@greg](https://gitlab.com/greg) | 1 | 500 |
| [@jacobvosmaer-gitlab](https://gitlab.com/jacobvosmaer-gitlab) | 2 | 400 |
| [@f_santos](https://gitlab.com/f_santos) | 3 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 4 | 300 |
| [@andrewn](https://gitlab.com/andrewn) | 5 | 200 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 6 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 7 | 30 |
| [@john.mcdonnell](https://gitlab.com/john.mcdonnell) | 8 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@NicoleSchwartz](https://gitlab.com/NicoleSchwartz) | 1 | 400 |
| [@vburton](https://gitlab.com/vburton) | 2 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 1200 |
| [@feistel](https://gitlab.com/feistel) | 2 | 400 |
| [@spirosoik](https://gitlab.com/spirosoik) | 3 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 4 | 200 |
| [@zined](https://gitlab.com/zined) | 5 | 200 |


